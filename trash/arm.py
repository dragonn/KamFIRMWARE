    #import RPi.GPIO as GPIO
import threading
import time
import queue
import crcmod
from cobs import cobs

class Arm():
    def __init__(self, port):
        self.crc16 = crcmod.predefined.mkCrcFun('modbus')
        self.port = port
        self.id = b'\x02'
        self.main = self.id + b'\x00\x00'
        self.main+=self.crc16(self.main).to_bytes(2, byteorder='big')
        self.timeout = time.time()
        self.queue = queue.Queue()

        self.modepwm = {
            'pwms' :[0, 0, 0, 0, 0, 0, 0, 0],
            'directions': [0, 0, 0, 0, 0, 0, 0, 0],
            'dirraw': 0x00,
        }
        self.mode = 0xFF
    def rec(self, packet):
        cmd = packet[2]
        #print(packet.hex())
        #print(len(packet))
        if cmd == 0:
            self.mode = packet[3]
            #print(self.mode)
            if self.mode == 0:
                self.modepwm['pwms']=list(packet[4:-3])
                #print(self.modepwm['pwms'])
                #print(len(packet))
                dir = list('{:08b}'.format(packet[12]))
                self.modepwm['dirraw']=packet[12]
                #print(packet[12])
                self.modepwm['directions']=[int(i) for i in dir]
                #print(self.modepwm['directions'])
                self.main=packet
                self.timeout=time.time()
            elif self.mode == 0xFF:
                '''self.main = self.id + b'\x00\x00'
                self.main+=self.crc16(self.main).to_bytes(2, byteorder='big')'''
                self.modepwm['pwms']=[0, 0, 0, 0, 0, 0, 0, 0]
                self.modepwm['directions']=[0, 0, 0, 0, 0, 0, 0, 0]
                self.mode = 0xFF
                self.timeout=time.time()
        else:    
            self.queue.put_nowait(packet)
        #
    def write(self):
        #print(self.mode)
        if time.time() - self.timeout > 0.1:
            self.error_radio()
        
        if self.mode == 0 or self.mode == 0xFF:
            #index = 0
            #pwm = self.modepwm['pwms'][0]
            '''for index, pwm in enumerate(self.modepwm['pwms']):
                if pwm == 0:
                    packet = self.id + b'\x00' + b'\x03' + (index+1).to_bytes(length=1, byteorder='big')+b'\x00'+b'\x00'
                else:
                    dir = self.modepwm['directions'][index]+1
                    packet = self.id + b'\x00' + b'\x03' + (index+1).to_bytes(length=1, byteorder='big')+dir.to_bytes(length=1, byteorder='big')+pwm.to_bytes(length=1, byteorder='big')
                
                packet+=self.crc16(packet).to_bytes(2, byteorder='big')
                self.port.write(cobs.encode(packet)+b'\x00')
                
                read=self.port.read(10)
                #print(read)
                #time.sleep(0.05)
                if len(read) == 0:
                    self.error_com()
                else:
                    print('arm answer rec')
                    #print(read.hex())
                    pass
                pass'''
            #print(self.modepwm['dirraw'])
            #print(bytes(self.modepwm['pwms']).hex())
            #print(bytes(self.modepwm['dirraw']))
            packet = self.id + b'\x00' + b'\x0b' + bytes(self.modepwm['pwms'])+self.modepwm['dirraw'].to_bytes(length=1, byteorder='big')
            #print(packet.hex())
            packet+= self.crc16(packet).to_bytes(2, byteorder='big')
            #print(packet.hex())
            #print(len(packet.hex()))
            self.port.write(cobs.encode(packet)+b'\x00')
            #self.port.reset_output_buffer()
            read=self.port.read(10)
            if len(read) == 0:
                self.error_com()
            else:
                #print('arm answer rec')
                #print(read.hex())
                pass
            pass
        #ignore
        elif self.mode == 0xFA:
            self.port.write(cobs.encode(self.main)+b'\x00')
            '''read=self.port.read(10)
            if len(read) == 0:
                self.error_com()
            else:
                print('arm answer rec')
                print(read.hex())
                pass

            #print(read)'''
        
    def read(self):
        pass

    def error_com(self):
        print('arm answer timeout')
        pass

    def error_radio(self):
        #print('error_radio')
        #print(''.join('{:02x} '.format(x) for x in self.main))
        self.modepwm['pwms']=[0, 0, 0, 0, 0, 0, 0, 0]
        self.modepwm['directions']=[0, 0, 0, 0, 0, 0, 0, 0]
        self.mode = 0xFF

        self.port.write(cobs.encode(b'\x02\x00\x00')+b'\x00')
        read=self.port.read(10)
        if len(read) == 0:
            self.error_com()
        else:
            #print('arm answer rec')
            #print(read.hex())
            pass
        pass
