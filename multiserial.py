import serial
import queue
import threading
import time
import crcmod
from cobs import cobs
class Multiserial(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.crc16 = crcmod.predefined.mkCrcFun('modbus')
        self.serial = serial.Serial('/dev/ttyAMA0', 115200, timeout=0.0001)
        #self.serial.write(b'\x00\x00\x00')
        #self.serial.close()
        #time.sleep(1)
        #self.serial = serial.Serial('/dev/ttyAMA0', 115200)
        
        self.queueout = queue.Queue()
        self.current = b''
    def run(self):
        self.current = b''
        while True:
            read=self.serial.read(1)
            #print(read)
            if read != b'':
                
                if read == b'\x00':
                    #print(current)
                    if self.current != b'':
                        self.queueout.put_nowait( self.current)
                        self.current = b''
                else:
                    self.current += read    

    def read(self, count):
        #self.queueout.clear()
        with self.queueout.mutex: self.queueout.queue.clear()
        try:
            packet = self.queueout.get(timeout=0.01)
            #print(packet)
            packet = cobs.decode(packet)
            crc = packet[-2:]
            if crc == self.crc16(packet[:-2]).to_bytes(2, byteorder='big'):
                return packet
            else:
                print('crc multiserial error')
                return b''
        except queue.Empty as e:
            #print(self.current)
            return b''
        except cobs.DecodeError as e:
            #print(e)
            #print(packet.hex())
            return b''
    
    def write(self, values):
        self.serial.write(values)
        self.serial.reset_output_buffer()
        time.sleep(0.001)
                    

if __name__ == '__main__':
    multiserial = Multiserial()
    multiserial.start()
    pass
