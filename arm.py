    #import RPi.GPIO as GPIO
import threading
import time
import queue
import crcmod
from cobs import cobs

class Arm():
    def __init__(self, port):
        self.crc16 = crcmod.predefined.mkCrcFun('modbus')
        self.port = port
        self.id = b'\x02'
        self.main = self.id + b'\x00\x00'
        self.main+=self.crc16(self.main).to_bytes(2, byteorder='big')
        self.main_old = b''
        self.old_count = 0
        self.timeout = time.time()
        self.queue = queue.Queue()
        self.queue_current = b''
        self.queue_count = 0
        self.modepwm = {
            'pwms' :[0, 0, 0, 0, 0, 0, 0, 0],
            'directions': [0, 0, 0, 0, 0, 0, 0, 0],
            'dirraw': 0x00,
        }

        self.modepid = {
            'pids': [0, 0, 0, 0],
            'pwms': [0, 0, 0, 0],
            'directions': [0, 0, 0, 0],
            'count': 0
        }


        self.modekin = {
            'kin': [0, 0],
            'pwms': [0, 0, 0, 0, 0],
            'directions': [0, 0, 0, 0, 0],
            'count': 0
        }
        self.mode = 0xFF
        self.position = b''
        self.adccount = 0

        
    def rec(self, packet):
        cmd = packet[2]
        #print(packet.hex())
        #print(len(packet))
        if cmd == 0:
            self.mode = packet[3]
            #print(self.mode)
            if self.mode == 0:
                self.modepwm['pwms']=list(packet[4:-3])
                #print(self.modepwm['pwms'])
                #print(len(packet))
                dir = list('{:08b}'.format(packet[12]))
                self.modepwm['dirraw']=packet[12]
                #print(packet[12])
                self.modepwm['directions']=[int(i) for i in dir]
                #print(self.modepwm['directions'])
                self.main=packet
                self.timeout=time.time()
            elif self.mode == 0x01:
                #pid
                self.modepid['pids'][0]=int.from_bytes(packet[4:6], byteorder='big', signed=True)
                self.modepid['pids'][1]=int.from_bytes(packet[6:8], byteorder='big', signed=True)
                self.modepid['pids'][2]=int.from_bytes(packet[8:10], byteorder='big', signed=True)
                self.modepid['pids'][3]=int.from_bytes(packet[10:12], byteorder='big', signed=True)

                self.modepwm['pwms']=list(packet[13:17])
                #print(self.modepwm['pwms'])

                dir = list('{:08b}'.format(packet[14]))
                #print(dir)
                #print("pid packet: " + packet.hex())
                #print(self.modepid['pids'])
                #print(packet[4+0:6+0].hex())
                pass
            elif self.mode == 0x02:
                #kin
                #02000001000000000000fe00025db5
                self.modekin['kin'][0]=int.from_bytes(packet[4:6], byteorder='big', signed=True)
                self.modekin['kin'][1]=int.from_bytes(packet[6:8], byteorder='big', signed=True)
                #print(self.modekin)
                #print("kin packet: " + packet.hex())
                pass
            elif self.mode == 0xFF:
                '''self.main = self.id + b'\x00\x00'
                self.main+=self.crc16(self.main).to_bytes(2, byteorder='big')'''
                self.modepwm['pwms']=[0, 0, 0, 0, 0, 0, 0, 0]
                self.modepwm['directions']=[0, 0, 0, 0, 0, 0, 0, 0]
                self.mode = 0xFF
                self.timeout=time.time()
        elif cmd == 0xFF:
            self.queue.put_nowait(b'\x02\x00\x00')
        else:   
            #print(packet.hex()) 
            self.queue.put_nowait(packet)
        #
    def write(self):
        #print(self.mode)
        if time.time() - self.timeout > 0.5:
            self.error_radio()
        
        if self.mode == 0 or self.mode == 0xFF:
            self.main = self.id + b'\x00' + b'\x0b' + bytes(self.modepwm['pwms'])+self.modepwm['dirraw'].to_bytes(length=1, byteorder='big')
            self.main += self.crc16(self.main).to_bytes(2, byteorder='big')
        #print(self.modepid['pids'])

        if self.mode == 1:    
            '''if self.modepid['pids'][self.modepid['count']] != 0:
                id = self.modepid['count'] + 1
                self.main = self.id + b'\x00' + b'\x08' + id.to_bytes(length=1, byteorder='big') + self.modepid['pids'][self.modepid['count']].to_bytes(length=4, byteorder='big', signed=True)
                self.main += self.crc16(self.main).to_bytes(2, byteorder='big')
            else:
                self.main = b''
            self.modepid['count']+=1
            if self.modepid['count'] > 3:
                self.modepid['count']=0'''

            for i in range(0, 4):
                if self.modepid['pids'][i] != 0:
                    id=i+1
                    self.main = self.id + b'\x00' + b'\x08' + id.to_bytes(length=1, byteorder='big') + self.modepid['pids'][i].to_bytes(length=4, byteorder='big', signed=True)
                    self.main += self.crc16(self.main).to_bytes(2, byteorder='big')
                    self.port.write(cobs.encode(self.main)+b'\x00')
                    read=self.port.read(10)
                    if len(read) == 0:
                        self.error_com()
                        #print('\n\r')
                    else:
                        pass

            self.main = b''
            pass
        
        if self.mode == 2:
            #if self.modepid['pids'][self.modepid['count']]
            print(self.modekin['kin'])
            pwm = 128
            pwm = pwm.to_bytes(length=1, byteorder='big', signed=False)
            kin0 = self.modekin['kin'][0].to_bytes(length=4, byteorder='big', signed=True)
            kin1 = self.modekin['kin'][1].to_bytes(length=4, byteorder='big', signed=True)

            self.main = self.id + b'\x00' + b'\x02'  + kin0 + kin1 + pwm
            self.main += self.crc16(self.main).to_bytes(2, byteorder='big')
            pass

        if self.main != b'':
            if self.main != self.main_old or self.old_count < 5:
                #print('write: '+ self.main.hex())
                self.port.write(cobs.encode(self.main)+b'\x00')
                read=self.port.read(10)
                if len(read) == 0:
                    self.error_com()
                    #print('\n\r')
                else:
                    #print('arm answer rec')
                    #00020b036f00fc1e600aeec3db
                    #print(read.hex())
                    #print('\n\r')
                    self.main_old = self.main
                    self.parse(read)
            else:
                self.old_count+=1

        if not self.queue.empty() and self.queue_current == b'':
            self.queue_current = self.queue.get()
            self.queue_current += self.crc16(self.queue_current).to_bytes(2, byteorder='big')
            self.queue_count = 0

        if self.queue_current != b'':
            #print("arm queue packet")
            #print(self.queue_current.hex())
            self.port.write(cobs.encode(self.queue_current)+b'\x00')
            read=self.port.read(10)
            if len(read) == 0:
                self.error_com()
                self.queue_count += 1
                #print('\n\r')
            else:
                #print(read.hex())
                self.queue_current = b''

            if self.queue_count > 5:
                print("arm confrim timeout")
                self.queue_current = b''

        if self.adccount > 20:
            self.adccount=0

            packetadc = b'\x02\x00\x07'
            packetadc += self.crc16(packetadc).to_bytes(2, byteorder='big')
            self.port.write(cobs.encode(packetadc)+b'\x00')
            read=self.port.read(10)
            if len(read) == 0:
                print('adc timeout')
                self.error_com()
            else:
                #print('adc rec')
                #print(read.hex())
                pass
        else:
            self.adccount+=1
        
        
    def read(self):
        pass

    def error_com(self):
        print('arm answer timeout')
        pass

    def error_radio(self):
        #print('error_radio')
        #print(''.join('{:02x} '.format(x) for x in self.main))
        self.modepwm['pwms']=[0, 0, 0, 0, 0, 0, 0, 0]
        self.modepwm['directions']=[0, 0, 0, 0, 0, 0, 0, 0]
        #self.mode = 0xFF

        '''self.port.write(cobs.encode(b'\x02\x00\x00')+b'\x00')
        read=self.port.read(10)
        if len(read) == 0:
            self.error_com()
        else:
            #print('arm answer rec')
            #print(read.hex())
            pass
        pass'''

    def parse(self, packet):
        cmd = packet[2]
        #print(cmd)
        if cmd == 0x0b:
            packet = packet[:-2]
            self.position = b''
            self.position+=packet[3:5]
            self.position+=packet[5:7]
            self.position+=packet[7:9]
            self.position+=packet[9:11]
            #print(packet[4:6].hex())

            #self.position[0]=int.from_bytes(packet[3:5], byteorder='big')
            #self.position[1]=int.from_bytes(packet[5:7], byteorder='big')
            #self.position[2]=int.from_bytes(packet[7:9], byteorder='big')
            #self.position[3]=int.from_bytes(packet[9:11], byteorder='big')
            #print(self.position)
        pass

    def get(self):
        return self.position

    def get_adc(self):
        pass
