import numpy as np
import math

class Inertia():
    def __init__(self, start):
        self.current = start
        #self.file = open("testfile.txt","w") 
        self.qold = [0,0,0,0]

    def calc(self, qnew):
        q = [0,0,0,0]
        q[0]=qnew[0]-self.qold[0]
        q[1]=qnew[1]-self.qold[1]
        q[2]=qnew[2]-self.qold[2]
        q[3]=qnew[3]-self.qold[3]
        
        if q[0] != 0 or q[1] != 0 or q[2] != 0 or q[3] != 0:
            self.qold = qnew
            #print(q)
            v=self.current
            vx=q[1]
            vy=q[2]
            vz=q[3]
            
            #print('vx: {}, vy: {}, vz: {}'.format(vx, vy, vz))

            #theta=math.radians(q[0])
            theta=q[0]
            #print('theta: {}'.format(theta))
            #print(math.degrees(theta))
            s0 = math.cos(theta/2)
            #print('s0: {}'.format(s0))

            v0 = math.sqrt(pow(vx, 2) + pow(vy, 2) + pow(vz, 2))
            #print('v0: {}'.format(v0))

            x0 = (vx/abs(v0))*math.sin(theta)
            y0 = (vy/abs(v0))*math.sin(theta)
            z0 = (vz/abs(v0))*math.sin(theta)
            
            #print('x0: {}, y0: {}, z0: {}'.format(x0, y0, z0))

            m = np.matrix([
                [2*(pow(s0, 2)+pow(x0,2))-1, 2*(x0*y0-s0*z0), 2*(s0*y0+x0*z0)],
                [2*(s0*z0+x0*y0), 2*(pow(s0, 2)+pow(y0, 2))-1, 2*(y0*z0-s0*x0)],
                [2*(x0*z0-s0*y0), 2*(s0*x0+y0*z0), 2*(pow(s0, 2)+pow(z0, 2))-1],
            ])

            #print(m)
            
            v = np.matrix([
                [v[0]],
                [v[1]],
                [v[2]],
            ])

            vr=np.matmul(m, v)
            #print(vr)
            self.current[0]=vr.item(0, 0)
            self.current[1]=vr.item(1, 0)
            self.current[2]=vr.item(2, 0)
            #return self.current
            print(self.current)

    def get(self):
        return self.current
        #return [ round(self.current[0], 4), round(self.current[1], 4), round(self.current[2], 4) ]

#position = Inertia([1,0,0])
#print(position.calc([math.radians(90),1,1,1]))
#print(position.calc([math.radians(90),1,1,1]))
'''        
def quation_calc(q, v):
	vx=q[1]
	vy=q[2]
	vz=q[3]
	
	theta=q[0]/2
	s0 = math.cos(theta)
	
	v0 = math.sqrt(pow(vx, 2) + pow(vy, 2) + pow(vz, 2))
	
	x0 = (vx/abs(v0))*math.sin(theta)
	y0 = (vy/abs(v0))*math.sin(theta)
	z0 = (vz/abs(v0))*math.sin(theta)
		
	#m = np.matrix([
	#	[( 1 - 2*pow(q[2], 2) - 2*pow(q[3], 2)), 2 * ( q[1]*q[2] + q[0]*q[3]), 2 * ( q[1]*q[3] - q[0]*q[2] )],
	#	[2 * ( q[1]*q[2] - q[0]*q[3] ), ( 1 - 2*pow(q[1], 2) - 2*pow(q[3], 2)), 2 * ( q[2]*q[3] + q[0]*q[1] )],
	#	[2 * ( q[1]*q[3] + q[0]*q[2]), 2 * ( q[2]*q[3] - q[0]*q[1] ), ( 1 - 2*pow(q[1], 2) - 2*pow(q[2],2))],
	#])
	
	
	m = np.matrix([
		[2*(pow(s0, 2)+pow(x0,2))-1, 2*(x0*y0-s0*z0), 2*(s0*y0+x0*z0)],
		[2*(s0*z0+x0*y0), 2*(pow(s0, 2)+pow(y0, 2))-1, 2*(y0*z0-s0*x0)],
		[2*(x0*z0-s0*y0), 2*(s0*x0+y0*z0), 2*(pow(s0, 2)+pow(z0, 2))-1],
	])
	
	v = np.matrix([
		[v[0]],
		[v[1]],
		[v[2]],
	])
	
	vr=np.matmul(m, v)
	return vr
	pass


print(quation_calc([math.radians(90),1,1,1], [1,0,0]))
'''
