import time
import queue
import crcmod
from cobs import cobs

class Console():
    def __init__(self, port):
        self.crc16 = crcmod.predefined.mkCrcFun('modbus')
        self.port = port
        self.id = b'\x02'
        self.main = self.id + b'\x00\x00'
        self.main+=self.crc16(self.main).to_bytes(2, byteorder='big')
        self.timeout = time.time()
        self.queuein = queue.Queue()
        self.queueout = queue.Queue()
        #print(self.queueout)

    def rec(self, packet):
        print(packet)
        cmd = packet[2]
        if cmd == 0:
            packet = packet[3:-2]
            #print(packet)
            packet += self.crc16(packet).to_bytes(2, byteorder='big')
            packet = cobs.encode(packet)+b'\x00'
            #print(packet)
            self.queuein.put_nowait(packet)
        
    def write(self):        
        while not self.queuein.empty():
            packet=self.queuein.get()
            #print("console start")
            #print(packet)
            self.port.write(packet)
            #self.port.reset_output_buffer()
            #time.sleep(0.001)
            #print("console odczyt")
            read=self.port.read(40)
            #print("console odczyt end")
            #print(read)
            #print(len(read))
            if len(read) > 1:
                self.queueout.put(read)
            else:
                self.error_com()
                self.queueout.put(b'\xFF\xFF')
                

        
    def get(self):
        if not self.queueout.empty():
            return self.queueout.get()
        else:
            return False
        

    def error_com(self):
        print('console answer timeout')

    def error_radio(self):
        #print('error_radio')
        #print(''.join('{:02x} '.format(x) for x in self.main))
        #self.main = b'\x01\x00\x00\x5A\x5A\x00\x00'
        #self.main+=self.crc16(self.main).to_bytes(2, byteorder='big')
        pass