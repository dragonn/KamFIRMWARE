import serial
import threading
import crcmod
import queue
import time
from cobs import cobs

class Radio(threading.Thread):
    def __init__(self, recfunctions):
        threading.Thread.__init__(self)
        
        self.ser = serial.Serial('/dev/ttyUSB0', 115200)
        self.buffer = b''
        self.queue = queue.Queue()
        self.crc16 = crcmod.predefined.mkCrcFun('modbus')
        self.values = {}
        self.rssi = 0
        self._exiting = False
        self.recfunctions = recfunctions

        self.packetolds = {

        }

    def run(self):
        time.sleep(1)
        conf=self.ser.read(self.ser.inWaiting())
        #print(conf.decode('utf-8'))
        print(conf)
        self.ser.flushInput()
        self.ser.flushOutput()

        read_thread = threading.Thread(target=self._read, args=())
        read_thread.start()
        while not self._exiting:
            #if self.ser.inWaiting():
            #    self.buffer+=self.ser.read(self.ser.inWaiting())
            #    terminator=self.buffer.find(b'\x00')
            #    if terminator != -1:
            #        self.handle(self.buffer[:terminator])
            #        self.buffer=self.buffer[terminator+1:]
            
            if not self.queue.empty():
                while not self.queue.empty():
                    packet=self.queue.get()
                    #print(packet)
                    self.ser.write(packet)
            else:
                time.sleep(0.01)
        
        pass

    def _read(self):
        while not self._exiting:
            if self.ser.inWaiting():
                self.buffer+=self.ser.read(self.ser.inWaiting())
                terminator=self.buffer.find(b'\x00')
                if terminator != -1:
                    #print(self.buffer[:terminator])
                    self.handle(self.buffer[:terminator])
                    self.buffer=self.buffer[terminator+1:]
            else:
                time.sleep(0.005)
    
    def handle(self, packet):
        packet = cobs.decode(packet)
        #loopback fix
        #print(''.join('{:02x} '.format(x) for x in packet))
        #packet=packet[:-2]
        #print(packet)
        self.rssi = int.from_bytes(packet[-2:], byteorder='little', signed=True)
        if len(packet) > 3: 
            id = packet[0]
            #print(id)
            cmd = packet[1]
            crc = packet[-4:-2]

            if crc == self.crc16(packet[:-4]).to_bytes(2, byteorder='big'):
                if id != 0xFE:
                    if id in self.recfunctions:
                        self.recfunctions[id](packet[:-2])
                    else:
                        print('unknow id')
                else:
                    print('confrim packet rec')
                    print(packet.hex())
                    #fe000001020a0101e912deff
                    key = packet[3]
                    retries = packet[4]
                    id2 = packet[5]
                    print(id2)
                    if id2 in self.recfunctions:
                        print('confrim packet process')
                        self.recfunctions[id2](packet[5:-4])
                        self.put(0xFE, 0x01, key.to_bytes(1, byteorder='big')+retries.to_bytes(1, byteorder='big'))
                    else:
                        print('unknow id')

                #self.values[ str(id) + 'cmd' + str(cmd) ] = list(packet[2:-2])
                #print(list(packet[2:-2]))
            else:
                #raise ValueError("CRC decode error")
                print("CRC decode error")
            pass
        
    def put(self, id, cmd, values):
        if not id in self.packetolds:
            self.packetolds[id]={}

        if not cmd in self.packetolds[id]:
            self.packetolds[id][cmd]={
                'old_count': 0,
                'values': b''
            }
            
        if self.packetolds[id][cmd]['values'] != values or self.packetolds[id][cmd]['old_count'] > 5:
            packet=id.to_bytes(1, byteorder='big')+cmd.to_bytes(1, byteorder='big')+bytes(values)
            packet+=self.crc16(packet).to_bytes(2, byteorder='big')
            packet=cobs.encode(packet)+b'\x00'
            self.queue.put_nowait(packet)

            self.packetolds[id][cmd]['values']=values
            self.packetolds[id][cmd]['old_count']=0
        else:
            self.packetolds[id][cmd]['old_count']+=1
        pass
        
    def get(self, id, cmd):
        if id+'cmd'+cmd in self.values:
            return self.values[ id+'cmd'+cmd ]
        else:
            return []

    def __del__(self):
        self._exiting = True
            
if __name__ == '__main__':
    radio = Radio()
    radio.start()
    
    pass
