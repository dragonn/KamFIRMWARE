#import RPi.GPIO as GPIO
import time
import queue
import crcmod
from cobs import cobs

class Driving():
    def __init__(self, port):
        self.crc16 = crcmod.predefined.mkCrcFun('modbus')
        self.port = port
        self.main = bytearray.fromhex('0100005a5a0000')
        self.main_old = b''
        self.old_count = 0
        self.main+=self.crc16(self.main).to_bytes(2, byteorder='big')
        self.timeout = time.time()
        self.queue = queue.Queue()
    def rec(self, packet):
        #print(packet.hex())
        cmd = packet[2]
        if cmd == 0:
            self.main=packet
            self.timeout=time.time()
        elif cmd == 1:
            self.main = bytearray.fromhex('0100005a5a0000')
            self.main+=self.crc16(self.main).to_bytes(2, byteorder='big')
            self.timeout=time.time()
        else:
            self.queue.put_nowait(packet)
        #
    def write(self):
        if time.time() - self.timeout > 0.5:
            self.error_radio()

        if len(self.main) > 0:
            if self.main != self.main_old:
                #print(self.main)
                self.port.write(cobs.encode(self.main)+b'\x00')
                #self.port.reset_output_buffer()
                #time.sleep(0.001)
                #self.port.setRTS(True)
                #GPIO.output(self.pin, GPIO.LOW)
                read=self.port.read(10)
                #print('drving read')
                #print(read)
                if len(read) == 0:
                    self.error_com()
                else:
                    self.main_old = self.main
                    pass
            else:
                self.old_count+=1
                if self.old_count > 5:
                    self.main_old = b''
                    self.old_count = 0
            
        
    def read(self):
        pass

    def error_com(self):
        print('driving answer timeout')
        pass

    def error_radio(self):
        #print('error_radio')
        #print(''.join('{:02x} '.format(x) for x in self.main))
        self.main = bytearray.fromhex('0100005a5a0000')
        self.main+=self.crc16(self.main).to_bytes(2, byteorder='big')
        #print('error_radio')
        pass
