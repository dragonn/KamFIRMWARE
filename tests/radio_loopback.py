import serial
import time
from cobs import cobs
#print(cobs.encode(b'CONF')+ b'x00')

ser = serial.Serial(port='/dev/ttyUSB0', baudrate=115200)
time.sleep(1)
out=b''
while ser.inWaiting() > 0:
    out += ser.read(1)
print(out.decode('utf-8'))

#for i in range(0, 5):
#    message = cobs.encode(b'1234TEST'+str(i).encode('utf-8')+b'\n\r')+b'\x00'
#    ser.write(message)
#    time.sleep(0.2)

out=b''
time.sleep(1)
while ser.inWaiting() > 0:
    out += ser.read(1)
    if out[-1]==0:
        packet=cobs.decode(out[:-1])
        #print(packet)
        #print(packet[:-2])
        packet_str=packet[:-4].decode('utf-8')
        rssi=int.from_bytes(packet[-2:], byteorder='little', signed=True)
        print("{} rssi:{}\n".format(packet_str, rssi))
        out=b''
        
for i in range(0, 100):
    message_plain=b'TEST'+str(i).encode('utf-8')+b'END'
    message = cobs.encode(message_plain)+b'\x00'
    #print(message)
    ser.write(message)
    time.sleep(0.015)
    while ser.inWaiting() > 0:
        out += ser.read(1)
        #print(out)
        if out[-1]==0:
            packet=cobs.decode(out[:-1])
            #print(packet)
            if packet[:-4] != message_plain:
                print('ERROR')
            #print('wtf')
            #print(packet[:-4])
            #packet_str=packet[:-4].decode('utf-8')
            rssi1=int.from_bytes(packet[-2:], byteorder='little', signed=True)
            rssi2=int.from_bytes(packet[-4:-2], byteorder='little', signed=True)
            print("{} rssi1:{} rssi2:{}".format(packet, rssi1, rssi2))
            out=b''

print(out)
