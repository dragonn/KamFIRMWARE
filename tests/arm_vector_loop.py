import serial
from cobs import cobs
import crcmod
serial = serial.Serial('/dev/ttyAMA0', 115200, timeout=0.1)
crc16 = crcmod.predefined.mkCrcFun('modbus')


for i in range(0,10):
    x=50
    y=50
    pwm=127
    packet = b'\x02\x00\x02'+x.to_bytes(length=4, byteorder='big', signed=True)+y.to_bytes(length=4, byteorder='big', signed=True)+pwm.to_bytes(length=1, byteorder='big')
    packet+=crc16(packet).to_bytes(2, byteorder='big')
    serial.write(cobs.encode(packet)+b'\x00')

    packet = serial.read(20)
    print(packet.hex())

    print(cobs.decode(packet[1:-1]))