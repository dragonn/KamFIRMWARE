import serial
from cobs import cobs
import crcmod
serial = serial.Serial('/dev/ttyAMA0', 115200, timeout=1)
crc16 = crcmod.predefined.mkCrcFun('modbus')


packet = b'\x02\x00\x00'
packet+=crc16(packet).to_bytes(2, byteorder='big')
serial.write(cobs.encode(packet)+b'\x00')

packet = serial.read(20)
print(packet.hex())

print(cobs.decode(packet[1:-1]))