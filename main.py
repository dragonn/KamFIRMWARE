import sys
import os

os.system('mjpg_streamer -i "/usr/lib/mjpg-streamer/input_uvc.so -d /dev/video0 -r 640x480 -f 24" -o "/usr/lib/mjpg-streamer/output_http.so -w ./www -p 8080" -b')
os.system('mjpg_streamer -i "/usr/lib/mjpg-streamer/input_uvc.so -d /dev/video1 -r 640x480 -f 24" -o "/usr/lib/mjpg-streamer/output_http.so -w ./www -p 8081" -b')


print(sys.argv)
print(len(sys.argv))
if len(sys.argv) > 1 and sys.argv[1] == '-d':
    print('forking into background')
    try:
        pid = os.fork()
        if pid > 0:
                # exit first parent
                sys.exit(0)
        os.setsid()
        os.umask(0)
    except OSError as e:
        sys.stderr.write("fork #1 failed: %d (%s)\n" % (e.errno, e.strerror))
        sys.exit(1)




import bno055
import time

import radio
import driving
import console
import multiserial

import serial
import fcntl
import struct
import arm

#import  RPi.GPIO as GPIO
#GPIO.setwarnings(False)
#GPIO.setmode(GPIO.BCM)
#GPIO.setup(17, GPIO.ALT3)
#GPIO.output(16, GPIO.LOW)

#rs485 = serial.Serial('/dev/ttyAMA0', 115200, timeout=0.05)
rs485 = multiserial.Multiserial()
rs485.start()

Bno055 = bno055.BNO055()
Bno055.start()

Driving = driving.Driving(rs485)
Console = console.Console(rs485)
Arm = arm.Arm(rs485)

Radio = radio.Radio({
    1: Driving.rec,
    2: Arm.rec,
    255: Console.rec
})
Radio.start()

while True:
    
    
    #print(acl_data)
    #x=int.from_bytes(acl_data[0:2], byteorder='big', signed=True)
    #y=int.from_bytes(acl_data[2:4], byteorder='big', signed=True)
    #z=int.from_bytes(acl_data[4:6], byteorder='big', signed=True)
    #print('x:{} y:{} z:{}'.format(x, y, z))
    Driving.write()
    time.sleep(0.03)
    Arm.write()
    time.sleep(0.03)
    Console.write()

    acl_data=Bno055.get()
    console_data=Console.get()
    arm_data=Arm.get()

    Radio.put(10, 1, acl_data)
    if console_data != False:
        Radio.put(0xFF, 0, console_data)    

    if arm_data != b'':
        Radio.put(0x02, 0, arm_data)    
    #time.sleep(0.06)
    #print("loop")
    #print('main')
    #print(Radio.rssi)
    pass
